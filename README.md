This repo contains user-side tools for interacting with ChemOS. 

# Setup
This package depends on `petname`, `matplotlib`, and `hein_utilities`. The latter is a non-PyPI repository. To install 
this package, download the full repository from Gitlab and run `python setup.py install`. The tools have been tested 
on Windows 10, but should run on other operating systems. If you encounter compatibility issues, please reach out and 
we'll try to get it sorted. 

**Important**: to fully utilize the `io` and `dubug` modules, a `chemospath` environment variable must be set which 
points to a directory containing the parameters, in progress, and results folders for a Dropbox configuration of ChemOS. 

# Modules

## config
The `config` module contains configuration information which rarely change (e.g. folder naming conventions, 
automatic path retrieval). It also contains a folder monitoring class instance (`hein_utilities.files.Watcher`) 
which checks for files in the input folder. 

## io
This module contains methods for file I/O, primarily for converting the ChemOS pickle (`*.pkl`) files to a dictionary 
format and _vice versa_. There are also wrapper methods for retrieval of all the pickle files in a directory. 

## visualization
This module contains visualization tools for plotting, interacting, and inspecting conditions and results. The main 
methods and classes are detailed in the module documentation, and example scripts and pickle files are provided to 
showcase the functionality: 
- `InteractiveInspectionPlot`: `ex_interactive_inspection.py`
- `InteractiveParameterPlot`: `ex_interactive_param.py`
- static `parameter_plot`: `ex_parameter_plot.py`

## debug
This module contains tools for offline-debugging, and contains methods for creating dummy pickle files. 

