import os
from chemos_interface.io import load_and_sort
from chemos_interface.animation import animate_parameter_plot


# path to example files
examples_path = os.path.dirname(os.path.abspath(__file__))
folder = os.path.join(
    examples_path,
    'example_results',
)

# load pickle files and sort the experiments by timestamp
sorted_values = load_and_sort(folder)

# define the plot keys (each key yields one subplot)
plot_keys = [
    'Pd_mol%',
    'Rxn_temp',
    'P/Pd_ratio',
    'ArBA_equiv',
    'P_ligand',
]

# generate the parameter plot
ani = animate_parameter_plot(
    sorted_values,
    'E-PR AY',
    *plot_keys,
    dot_size=35,
)

# option to save if you want
ani.save(
    os.path.join(
        folder,
        f'parameter animation ({ani._interval} ms interval).mp4'
    ),
    writer='ffmpeg',
    bitrate=1000,
    codec='libx264',
)

# or save as a gif
# ani.save(
#     os.path.join(
#         folder,
#         f'parameter animation ({ani._interval} ms interval).gif'
#     ),
#     writer='imagemagick',
#     fps=5,
# )

# or show it
# pl.show()  # not necessary when executed in console
# the figure and axes objects can be caught and further manipulated as desired
