"""
An example file to illustrate how the chemos interface tools maybe be used to retrieve instructions and return results
 to/from ChemOS. This script is an example of asynchronous loops.

See loop_integration.md for additional details.

Prerequisites: see the "Setup" portion of the README and ensure that `chemospath` is defined as an environment variable.
"""
import random
import logging
import time
from chemos_interface.config import CONDITIONS_KEY, MEASUREMENTS_KEY
from chemos_interface.io import get_oldest_parameters, write_completed_parameters

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('Example async loop integration')


def perform_experiment(**parameters):
    """
    A dummy method which "performs" an experiment based on the provided ChemOS parameters.

    :param parameters: named key, value pairs provided by ChemOS
    :return: response value
    """
    # here you would set acquisition parameters, assign values, etc.
    for key, value in parameters.items():
        logger.info(f'performing action with {key}: {value}')
    # no return from this method.


def check_for_result() -> bool:
    """
    Example logic check which indicates the presence of new results from an experiment.

    :return:
    """
    # perform some logic check: are there new results?
    logger.info('Checking for new results')
    return True


def retrieve_results():
    """
    A dummy method which "retrieves" the results of an experiment to return to ChemOS

    :return: response value
    """
    # here you would perform file IO, instrument connection and request, etc. to retrieve the response value
    logger.info('Retrieving result from last experiment')
    return random.random()


# counters for tracking which data has been returned and which experiment is next
_data_counter = 1
_exp_counter = 1
# storage dictionary for in-progress experiments
_exp_dict = {}

# asynchronous example
while True:
    # attempt to get the newest parameters
    new_parameters = get_oldest_parameters(
        move_file=True,
    )
    if new_parameters is not None:  # if a new parameter file was present, the return will be a dict
        logger.info('New parameters retrieved, initiating experiment')
        # store new parameters in an ongoing experiment
        _exp_dict[_exp_counter] = new_parameters
        perform_experiment(
            **new_parameters[CONDITIONS_KEY]  # the conditions for the experiment are in a subdictionary
        )
        # increment experiment counter
        _exp_counter += 1

    # if there are new results
    elif check_for_result() is True:
        logger.info('New results detected, retrieving and return result to ChemOS')
        # retrieve results
        result = retrieve_results()
        # associate result with experiment
        _exp_dict[_data_counter][MEASUREMENTS_KEY] = result
        # writes the completed parameters to file and moves the file to the Results folder for ChemOS to read
        write_completed_parameters(
            _exp_dict[_data_counter]
        )
        # increment data counter
        _data_counter += 1

    # if neither condition was met, wait briefly and reloop
    else:
        logger.info('No new parameters or result, pausing then reiterating')
        time.sleep(1)
