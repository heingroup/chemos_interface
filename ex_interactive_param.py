"""
An example script to show how to use the interactive parameter plot visualization tool.

This tools is similar to the parameter_plot tool, but is interactive. The cursor can be used to lasso points of interest,
which will then be colour-coded across all the subplots and the parameters will be printed in the console.
This plot can be helpful for visualizing and inspecting the distribution of values in relation to some response factor.
"""
import os
import pylab as pl
from chemos_interface.visualization import InteractiveParameterPlot
from chemos_interface.io import load_and_sort

# path to example files
examples_path = os.path.dirname(os.path.abspath(__file__))
folder = os.path.join(
    examples_path,
    'example_results',
)

# load pickle files and sort the experiments by timestamp
sorted_values = load_and_sort(folder)

# define the plot keys (each key yields one subplot)
plot_keys = [
    'Pd_mol%',
    'Rxn_temp',
    'P/Pd_ratio',
    'ArBA_equiv',
    'P_ligand'
]

# generate an Interactive Object
ipp = InteractiveParameterPlot(
    sorted_values,
    'E-PR AY',
    *plot_keys,
)
pl.show()  # not necessary when executed in console
