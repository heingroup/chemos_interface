"""
An example script to show how to use the interactive inspection plot visualization tool.

This tool plots experiment number versus a response factor in an interactive window. The cursor can be used to lasso
points of interest, which will then be colour-coded and a parameter plot with those points highlighted will be
generated, and the parameters will be printed in the console.
This plot can be helpful for inspecting values of interest as the ChemOS experiment progresses.

Note that the parameter plots will only be generated when this is executed in console.
todo: fix this ^
"""
import os
import pylab as pl
from chemos_interface.visualization import InteractiveInspectionPlot
from chemos_interface.io import load_and_sort

# path to example files
examples_path = os.path.dirname(os.path.abspath(__file__))
folder = os.path.join(
    examples_path,
    'example_results',
)

# load pickle files and sort the experiments by timestamp
sorted_values = load_and_sort(folder)

# define the plot keys (each key yields one subplot)
plot_keys = [
    'Pd_mol%',
    'Rxn_temp',
    'P/Pd_ratio',
    'ArBA_equiv',
    'P_ligand'
]

# generate an Interactive Object
iip = InteractiveInspectionPlot(
    sorted_values,
    'E-PR AY',
    *plot_keys,
)
pl.show()  # not necessary when executed in console
