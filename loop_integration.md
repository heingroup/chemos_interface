A companion readme for the `ex_loop_integration.py` script. 

Prerequisites: see the "Setup" portion of the README and ensure that `chemospath` is defined as an environment variable. 

This script serves as a barebones example for integrating a script with ChemOS. A dummy `perform_experiment` method is 
provided as an example of how ChemOS parameters are applied. 

# Loop Structures

The two primary loop structures are synchronous and asynchronous. The former is when instructions are retrieved, 
immediately executed, and the output is immediately returned. The latter is when other actions are performed between 
retrieval and return (or when multiple instruction sets are being managed. 

## Synchronous

This is an uncommon use case, as it blocks all other actions while the loop is being performed. It is generally 
recommended to use an asynchronous loop even if synchronous loops would suffice. However, this loop type is useful 
when there is no mechanism outside of the worker method to retrieve responses.   

The general structure for this loop is
1) Retrieve experimental parameters from ChemOS
2) Perform the experiment with those parameters
3) Return the response value to ChemOS

## Asynchronous

The asynchronous loop is more general but is more complicated to configure as details of ongoing experiments must be 
retained within the script. One important caveat for the example provided is that it assumes that the next returned 
response corresponds to the next experiment that was queued (FIFO). If assumption is invalid, you must apply an 
additional logic layer to match response to experiment (this functionality is in development for this code base 
but is incomplete). 

The general structure of this loop is similar to that of the synchronous loop, but is controlled by logic statements 
(`if` and `elif` catches). We recommend installing one logic catch for each distinct action, and terminating the logic 
chain with an `else` which performs a `time.sleep()` before reiterating. At a minimum, the following logic catches 
are required: 
- Retrieve new experimental parameters from ChemOS
- Return the response value to ChemOS

Additional steps associated with retrieving parameters, retrieving results, and writing data are required in order to 
correctly associate experiment with result. 