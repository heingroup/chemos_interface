import pickle
import os
import warnings
from typing import List
from datetime import datetime
from hein_utilities.files import Watcher

from .config import chemos_status_watch, input_watch, CHEMOSPATH, IN_PROGRESS_FOLDER, OUTPUT_FOLDER, CONDITIONS_KEY, PURGATORY_FOLDER, file_already_processed

if CHEMOSPATH is None:
    raise ValueError('A file path for ChemOS could not be found. Please define a "chemospath" environment variable '
                     'or add a definition to your .env file in the root of your project. ')


def retrieve_contents(filepath, keys=None):
    """
    Retrieves ChemOS dictionaries from pickle file

    :param filepath: path to file
    :param keys: keys to retrieve (if None, all keys will be returned)
    :return: dictionary present in file
    """
    with open(filepath, 'rb') as openpickel:
        out = pickle.load(openpickel)
    if keys is None or len(keys) == 0:
        return out
    return {
        key: out[key] for key in keys
    }


def check_chemos_status():
    """checks the status of chemos"""
    with open(chemos_status_watch.contents[0], 'rb') as file:
        return pickle.load(file)['status']


def move_pickle_file(from_path: str, to_folder: str, duplicate_action='purgatory'):
    """
    Moves a pickle file from the specified path to the target folder. If the file already exists in the target folder,
    the duplicate action will be performed.

    :param from_path: path to find the current location of the file
    :param to_folder: path to move the file to (not including the name)
    :param duplicate_action: what to do if there is a duplicate file. Options: None to do nothing, 'purgatory' to
        move the file to purgatory, or 'delete' to remove the file.
    :return: path to where the file ended up
    """
    file_name = from_path.split(os.sep)[-1]
    try:
        os.rename(
            from_path,
            os.path.join(to_folder, file_name)
        )
    except FileExistsError:
        if duplicate_action not in [None, 'purgatory', 'delete']:
            duplicate_action = 'purgatory'

        # do nothing
        if duplicate_action is None:
            return from_path

        # move to purgatory folder
        elif duplicate_action == 'purgatory':
            to_folder = os.path.join(CHEMOSPATH, PURGATORY_FOLDER)
            if os.path.isfile(os.path.join(to_folder, file_name)):
                i = 1
                while os.path.isfile(os.path.join(to_folder, f'{i}-{file_name}')) is True:
                    i += 1
                file_name = f'{i}-{file_name}'

            os.rename(
                from_path,
                os.path.join(to_folder, file_name)
            )
            return os.path.join(to_folder, file_name)

        # remove file
        elif duplicate_action == 'delete':
            os.remove(from_path)
            return None


def get_oldest_parameters(wait_for=False, move_file=False):
    """
    Gets the oldest set of parameters from the conditions folder.

    :param bool wait_for: whether to wait for the appearance of a file.
    :param bool move_file: whether to move the file to the in progress folder.
    :return: dictionary stored in the oldest loaded_conditions file.
    :rtype: dict
    """
    if len(input_watch) == 0:  # if there is no file
        if wait_for is False:  # and wait for is False, return None
            return None
        input_watch.wait_for_presence()
    oldest_path = input_watch.oldest_instance()
    file_name = oldest_path.split(os.sep)[-1]
    # check if the file has already been processed
    if file_already_processed(file_name) is True:
        move_pickle_file(  # move the duplicate file to purgatory
            oldest_path,
            os.path.join(CHEMOSPATH, PURGATORY_FOLDER)
        )
        return get_oldest_parameters(  # get the next file
            wait_for=wait_for,
            move_file=move_file,
        )
    with open(oldest_path, 'rb') as inputfile:
        loaded_conditions = pickle.load(inputfile)
    if 'filename' not in loaded_conditions:  # if the filename is not in the file, store in dictionary
        loaded_conditions['filename'] = file_name
    if move_file is True:
        move_pickle_file(
            oldest_path,
            os.path.join(CHEMOSPATH, IN_PROGRESS_FOLDER),
            duplicate_action='purgatory',
        )
    return loaded_conditions


def get_parameters(wait_for=False, move_file=False):
    """Legacy name for get_oldest_parameters method"""
    warnings.warn('The get_parameters method has been renamed to get_oldest_parameters. Please change your method call',
                  DeprecationWarning,
                  stacklevel=2)
    return get_oldest_parameters(wait_for=wait_for, move_file=move_file)


def get_all_pickle_contents(
        path: str = os.path.join(CHEMOSPATH, OUTPUT_FOLDER),
        include_subfolders: bool = False,
) -> List[dict]:
    """
    Retrieves the contents of all pickle files in the target directory.

    :param path: target path
    :param include_subfolders: whether to include subfolders
    :return: list of chemos parameter dictionaries
    """
    target = Watcher(
        path,
        watchfor='.pkl',
        includesubfolders=include_subfolders,
    )
    # todo catch errors in retrieval?
    return [retrieve_contents(file) for file in target.contents]


def remove_pickle_file(filename):
    """
    Removes the specified conditions file from the conditions directory. If the file does not exist, nothing is modified.

    :param filename: name of the file to remove
    """
    # remove from input folder
    if os.path.isfile(os.path.join(input_watch.path, filename)):
        os.remove(
            os.path.join(input_watch.path, filename)
        )
    # remove from in progress folder
    if os.path.isfile(os.path.join(CHEMOSPATH, IN_PROGRESS_FOLDER, filename)):
        os.remove(
            os.path.join(CHEMOSPATH, IN_PROGRESS_FOLDER, filename)
        )


def write_completed_parameters(
        parameters,
        remove_conditions=True,
        filename=None,
):
    """
    Writes the parameters of a completed experiment to a file in the measurements folder. If no timestamp is included
    in the parameters dictionary, one will be added in this method.

    :param dict parameters: parameter dictionary to write
    :param bool remove_conditions: whether to remove the parameter file from the conditions folder
    :param str filename: filename to remove. If there is a `filename` key in the `parameters` dictionary,
        this need not be provided.
    """
    if filename is None and 'filename' not in parameters:
        raise ValueError('No filename was provided and the filename key is not in the parameters dictionary.')
    elif filename is None:
        filename = parameters['filename']
    # todo consider writing to current file then moving
    fullpath = os.path.join(CHEMOSPATH, OUTPUT_FOLDER, filename)
    if os.path.isfile(fullpath) is True:
        if input(
                f'The file {filename} already exists in the target directory, overwrite? Y/N'
        ).lower() not in ['y', 'yes']:
            print('User declined to continue, writing aborted')
            return

    if 'timestamp' not in parameters:
        parameters['timestamp'] = str(datetime.now())

    if remove_conditions is True:  # remove the input file
        remove_pickle_file(filename)

    with open(fullpath, 'wb') as outputfile:
        pickle.dump(
            parameters,
            outputfile,
        )


def sort_list(lst: List[dict], sort_key: str = 'timestamp'):
    """
    Sorts the list of dictionaries using the provided key.

    :param lst: list of dictionaries
    :param sort_key: key to sort with
    :return: sorted list of dictionaries
    """
    if sort_key in lst[0]:  # if key is top level
        return [dct for dct in sorted(lst, key=lambda x: x[sort_key])]
    elif sort_key in lst[0][CONDITIONS_KEY]:  # if the key is in the conditions dictionary
        return [dct for dct in sorted(lst, key=lambda x: x[CONDITIONS_KEY][sort_key])]
    else:
        raise ValueError(f'The sort_key "{sort_key}" was not found in either the top level or the conditions level of the '
                         f'dictionary.')


def load_and_sort(folder: str, sort_key: str = 'timestamp'):
    """
    Loads the contents of the pickle files and returns the sorted list of values based on the provided sorting key.

    :param folder: target folder
    :param sort_key: key to sort by (either a top-level or parameters key)
    :return: sorted list of pickled dictionaries
    """
    contents = get_all_pickle_contents(folder)  # retrieve contents of the folder
    return sort_list(contents, sort_key=sort_key)
