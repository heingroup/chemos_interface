import os
from tqdm import tqdm
import numpy as np
import pylab as pl
import matplotlib.animation as animation
import matplotlib.gridspec as gridspec
from collections import Counter
from .config import CONDITIONS_KEY, MEASUREMENTS_KEY
from .visualization import _auto_subplot_shape, minima_and_maxima


def animate_parameter_plot(data,
                           response_key,
                           *plot_keys,
                           interval=200,
                           # save=False,
                           subplot_layout=None,
                           dot_size=15,
                           ):
    """
    todo elaborate the description
    Animates the ChemOS parameter optimization

    :param data: list of dictionaries
    :param response_key: key to plot on the y-axis
    :param plot_keys: keys to plot (x-axis)
    :param interval: interval (ms) between frames
    :param save: whether to save the animation as an .mp4 file in the output folder
    :param dot_size: size in points for the plot to use. The next up value will be 2x this, the best value will be
        2x this + 10, and the previously shown points will be this value in size.
    :return: animation object
    """
    def calculate_ylim(marginfactor):
        """
        Fix for a scaling issue with matplotlibs scatterplot and small values.
        Takes in a pandas series, and a marginfactor (float).
        A marginfactor of 0.2 would for example set a 20% border distance on both sides.
        Output:[bottom,top]
        To be used with .set_ylim(bottom,top)
        """
        border = abs(max_dct[MEASUREMENTS_KEY][response_key] * marginfactor)
        maxlim = max_dct[MEASUREMENTS_KEY][response_key] + border
        minlim = -border
        return minlim, maxlim

    def animate(i):
        nonlocal vals, maxval, iterable_data
        # if newy is not None:  # skips first value
        #     yvals = np.append(yvals, newy)
        nextup = next(iterable_data)  # get next data
        # data[i]  # get next job
        newy = nextup[MEASUREMENTS_KEY][response_key]

        # check for new maximum values
        if newy > maxval:
            maxval = newy
            newmax = True  # flag for new max value
        else:
            newmax = False

        # update scatters
        for ind, key in enumerate(plot_keys):
            # todo support categorical x-values
            x_value = nextup[CONDITIONS_KEY][key]
            if type(x_value) is str:
                x_value = string_map[key][x_value]
            if vals[ind]['newval'] is not None:  # append last value
                vals[ind]['sequential'].append(vals[ind]['newval'])
                vals[ind]['valplot'].set_offsets(  # previous values
                    np.asarray(vals[ind]['sequential'])
                )
            vals[ind]['newval'] = [x_value, newy]  # current set of values
            if newmax is True:  # if there is a new value, change max value
                vals[ind]['maxplot'].set_offsets(np.asarray([x_value, maxval]))
            vals[ind]['current'].set_offsets(np.asarray([x_value, newy]))  # current value
        return [group['axes'] for group in vals]

    def progress_iterable(iterable):
        for val in tqdm(iterable, 'Rendering frame'):
            yield val

    def empty_init():  # empty init to prevent update from being called twice
        pass

    minmax = minima_and_maxima(data)
    max_dct = data[minmax[MEASUREMENTS_KEY][response_key]['max_index']]
    iterable_data = progress_iterable(data)

    if len(plot_keys) == 0:
        plot_keys = sorted(data[0][CONDITIONS_KEY])
    plot_keys = sorted(plot_keys)
    if len(plot_keys) == 0:  # if no parameters are specified, use all
        plot_keys = sorted(data[0][CONDITIONS_KEY])
    elif len(plot_keys) == 1:  # if only 1, cast to list
        plot_keys = [plot_keys]

    # extract ranges and check for string values
    range_dict = {}
    string_map = {}
    for key in plot_keys:
        values = [job[CONDITIONS_KEY][key] for job in data]
        if type(values[0]) is str:  # categorical values
            count = Counter(values)
            # create a value map for string keys
            string_map[key] = {key: ind for ind, key in enumerate(sorted(count.keys()))}
            # set range to integer values
            range_dict[key] = [-1, len(count)]
        else:
            span = abs(max(values) - min(values))
            range_dict[key] = [
                min(values) - span * 0.05,
                max(values) + span * 0.05,
            ]

    if subplot_layout is None:
        # automatically determine the number of subplots
        subplot_layout = _auto_subplot_shape(len(plot_keys))

    vals = [  # create value list
       {
            # todo use min value as well
            'maxval': 0.,  # the maximum value found
            'newval': None,  # the incoming value
            'sequential': [],  # the sequential list of values

        } for key in plot_keys
    ]

    fig = pl.figure(figsize=(19.20, 10.80))
    gs_outer = gridspec.GridSpec(1, 1)  # create outer grid for labelling purposes
    outer_sp = pl.Subplot(fig, gs_outer[0])  # create outer subplot instance
    fig.add_subplot(outer_sp)
    gs_inner = gridspec.GridSpecFromSubplotSpec(  # create image gridspec
        *subplot_layout,
        subplot_spec=gs_outer[0],
    )
    # add subplots to figure
    ax = []
    for subplotspec in gs_inner:
        subplot = pl.Subplot(fig, subplotspec)
        ax.append(subplot)
        fig.add_subplot(subplot)
    fs = 16
    maxval = 0.

    outer_sp.set_frame_on(False)  # hide outer frame and ticks
    outer_sp.set_xticks([])
    outer_sp.set_yticks([])
    outer_sp.set_ylabel(f'{response_key}\n\n', size=fs)

    yrange = calculate_ylim(0.05)
    for ind, key in enumerate(plot_keys):
        axes = ax[ind]
        axes.set_xlabel(
            f'{key}'
            # f'{units}'  # todo set units
            , size=fs
        )
        axes.set_xlim(*range_dict[key])
        axes.set_ylim(*yrange)
        if key in string_map:
            axes.set_xticks(list(string_map[key].values()))
            axes.set_xticklabels(sorted(string_map[key]))
            axes.tick_params(
                axis='x',
                labelsize=fs - 4,
                labelrotation=20,
            )
            axes.tick_params(
                axis='y',
                labelsize=fs,
            )

        else:
            axes.tick_params(labelsize=fs)
    # disable any axes that are unused
    if ind != len(ax) - 1:
        for ind in range(ind + 1, len(ax)):
            ax[ind].set_visible(False)

    fig.tight_layout()
    for ind, key in enumerate(plot_keys):  # associate axes with indicies and set initial state
        # todo can the two loops be consolidated?
        axes = ax[ind]
        vals[ind]['axes'] = axes
        vals[ind]['maxplot'] = axes.scatter(  # scatter for max value
            [],
            [],
            marker='o',
            s=2 * dot_size + 10,
            facecolors=(0, 0, 0, 0),
            edgecolors='r',
        )
        vals[ind]['valplot'] = axes.scatter(  # scatter for values
            [],
            [],
            marker='o',
            s=dot_size,
            facecolors=(0, 0, 1, 0.5),
        )
        vals[ind]['current'] = axes.scatter(  # incoming value
            [],
            [],
            marker='o',
            s=dot_size * 2,
            facecolors='orange',
            edgecolors='k',
            # animated=True,
        )
        # todo figure out how to dynamically adjust subplot
        # if key in string_map:
        #     # adjust of the subplot
        #     current_position = axes.get_position()
        #     delta = 0.07
        #     axes.set_position([
        #         current_position.min[0],
        #         current_position.min[1] + delta,
        #         current_position.width,
        #         current_position.height - delta,
        #     ])

    ani = animation.FuncAnimation(
        fig,
        animate,
        frames=len(data),
        interval=interval,
        # blit=True,  # doesn't work with figure saving
        repeat=False,
        init_func=empty_init,
    )
    # todo consider reenabling saving method? (or let the user choose to save or show)
    # if save is True:
    #     ani.save(
    #         os.path.join(
    #             folder,
    #             'run parameters animation (%d ms).mp4' % interval,
    #         ),
    #         # writer='imagemagick'
    #         writer='ffmpeg',
    #         bitrate=1000,
    #         codec='libx264',
    #     )
    return ani


def animate_key(key='average', forced_ylabel='peak area (a.u.)', interval=200, save=False):
    """
    Animates the ChemOS parameter optimization

    :param key: the key to plot on the y-axis
    :param forced_ylabel: overrides the use of the key name for the y axis label
    :param interval: interval (ms) between frames
    :param save: whether to save the animation as an .mp4 file in the output folder
    :return: animation object
    """

    def set_axlims(minv, maxv, marginfactor=0.02):
        """
        Fix for a scaling issue with matplotlibs scatterplot and small values.
        Takes in a pandas series, and a marginfactor (float).
        A marginfactor of 0.2 would for example set a 20% border distance on both sides.
        Output:[bottom,top]
        To be used with .set_ylim(bottom,top)
        """
        border = abs((maxv - minv) * marginfactor)
        maxlim = maxv + border
        minlim = minv - border
        return minlim, maxlim

    def animate(i):
        nonlocal xys, maxval
        nextup = xys[i]  # get next job
        newy = nextup[1]

        # check for new maximum values
        if newy > maxval:
            maxval = newy
            maxsc.set_offsets(np.asarray(nextup))

        if i > 0:
            sc.set_offsets(np.asarray(xys[:i]))
        currsc.set_offsets(np.asarray(nextup))

        return ax

    raise NotImplementedError('Lars still needs to fix this method to work with general data')

    yvals = [job[key] for job in timesort]
    xvals = list(range(1, len(yvals) + 1))

    xys = list(zip(xvals, yvals))  # xy values

    fs = 16
    maxval = 0.
    # initial state of figure
    fig, ax = pl.subplots(
        1,
        1,
        figsize=(19.20, 10.80)
    )
    if forced_ylabel is not None:
        ax.set_ylabel(forced_ylabel, size=fs)
    else:
        ax.set_ylabel(key, size=fs)
    ax.set_xlabel('injection #', size=fs)
    ax.set_xlim(*set_axlims(1, xvals[-1]))
    ax.set_ylim(*set_axlims(
        min(yvals),
        max(yvals)
    ))
    ax.tick_params(labelsize=fs)
    sc = ax.scatter(  # value scatter
        [],
        [],
        marker='o',
        s=15,
        facecolors=(0, 0, 1, 0.5),
    )
    maxsc = ax.scatter(  # max set
        [],
        [],
        marker='o',
        s=40,
        facecolors=(0, 0, 0, 0),
        edgecolors='r',
    )
    currsc = ax.scatter(
        [],
        [],
        marker='o',
        s=30,
        facecolors='orange',
        edgecolors='k',
    )
    pl.tight_layout()
    interval = interval

    ani = animation.FuncAnimation(
        fig,
        animate,
        frames=len(timesort),
        interval=interval,
        # blit=True,  # doesn't work with figure saving
        repeat=False,
    )
    if save is True:
        ani.save(
            os.path.join(
                folder,
                'run %s animation (%d ms).mp4' % (key, interval),
            ),
            writer='ffmpeg',
            bitrate=1000,
            codec='libx264',
        )
    return ani
