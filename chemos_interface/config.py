import os
from hein_utilities.files import Watcher

if os.getenv('chemospath') is None:
    raise ValueError('A file path for ChemOS could not be found. Please define a "chemospath" environment variable '
                     'or add a definition to your .env file in the root of your project. ')


CHEMOSPATH = os.getenv('chemospath')

INPUT_FOLDER = 'Parameters'  # name of input folder
OUTPUT_FOLDER = 'Results'  # name of output folder
IN_PROGRESS_FOLDER = 'In Progress'  # name of folder for files in progress
CONFIGURATION_FOLDER = 'Configurations'  # name of the configurations folder
PURGATORY_FOLDER = 'Purgatory'  # name of the folder where pickle files are placed in purgatory

# create folders if they do not exist
for folder in [PURGATORY_FOLDER, INPUT_FOLDER, IN_PROGRESS_FOLDER, OUTPUT_FOLDER]:
    full_path = os.path.join(CHEMOSPATH, folder)
    if os.path.isdir(full_path) is False:
        os.mkdir(full_path)

CONDITIONS_KEY = 'processes'  # name of the dictionary key containing conditions and values
MEASUREMENTS_KEY = 'properties'  # name of the dictionary key where the output values will be stored

input_watch = Watcher(  # create Watcher instance for the input folder
    os.path.join(CHEMOSPATH, INPUT_FOLDER),
    '*.pkl'
)

# watcher instance to check whether a file has already been processed
processed_watch = Watcher(
    CHEMOSPATH,
    '*.pkl',
    exclude_subfolders=[INPUT_FOLDER],
)

chemos_status_watch = Watcher(  # watcher for ChemOS status file
    CHEMOSPATH,
    'ChemOS_status.pkl',
    includesubfolders=False,
)

# todo load list of configuration files or create Experiment instances


def file_already_processed(file_name: str) -> bool:
    """
    Checks if the file has already been processed (if it exists in the in progress or results directories)

    :param file_name: file name to check for
    :return: whether the file already exists
    """
    for file in processed_watch:
        if file.endswith(file_name):  # assumes that the filename will be completely unique
            return True
    return False

