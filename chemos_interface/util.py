import os
import shutil
from .config import CHEMOSPATH, OUTPUT_FOLDER
from .io import retrieve_contents
from hein_utilities.files import Watcher


def organize_directory(path=os.path.join(CHEMOSPATH, OUTPUT_FOLDER)):
    """
    Organizes the pickle files in the target directory, moving them into subfolders denoted by the session ID

    :param path: path to target directory
    """
    watch = Watcher(
        path,
        '*.pkl',
        includesubfolders=False,
    )

    for file in watch.contents:
        contents = retrieve_contents(file)
        target_folder = contents["session_id"]  # todo consider time stamping as well
        target_folder_path = os.path.join(CHEMOSPATH, OUTPUT_FOLDER, target_folder)
        if os.path.isdir(target_folder_path) is False:
            os.mkdir(target_folder_path)
        shutil.move(
            file,
            f'{target_folder_path}\\{contents["filename"]}'
        )
