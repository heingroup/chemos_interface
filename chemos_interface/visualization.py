__doc__ = "Visualization tools for ChemOS runs"

import os
import matplotlib
import pylab as pl
from typing import List
from matplotlib.widgets import Lasso
from matplotlib.collections import CircleCollection
import matplotlib.gridspec as gridspec
from matplotlib import colors as mcolors
import matplotlib.path as path
from .io import sort_list
from .config import CHEMOSPATH, CONDITIONS_KEY, MEASUREMENTS_KEY


class InteractiveInspectionPlot(object):
    def __init__(self,
                 data: list,
                 response_key: str,
                 *plot_keys,
                 ):
        """
        Creates an interactive plot of injection # vs the supplied key. The points can be lassoed to highlight values.
        The details of those values will be printed in the console, and a parameterplot with those highlighed values
        will be generated

        :param data: source data
        :param response_key: response key plot
        :param plot_keys: keys to plot for parameter plots
        """
        self.data = sort_list(data)  # assume unsorted
        self.response_key = response_key
        self.fig, self.ax = pl.subplots(
            1,
            figsize=(9.6, 5.4),
        )

        self.canvas = self.ax.figure.canvas
        if response_key in self.data[0][MEASUREMENTS_KEY]:
            self.ydata = [job[MEASUREMENTS_KEY][response_key] for job in self.data]
        elif response_key in self.data[0][CONDITIONS_KEY]:
            self.ydata = [job[CONDITIONS_KEY][response_key] for job in self.data]
        self.xdata = [i + 1 for i, val in enumerate(self.ydata)]
        if len(plot_keys) == 0:
            self.plot_keys = sorted(data[0][CONDITIONS_KEY])
        elif len(plot_keys) == 1:
            self.plot_keys = [plot_keys]
        else:
            self.plot_keys = plot_keys

        facecolors = [(0, 0, 0, 0) for d in self.xdata]
        if type(self.ydata[0]) == str:  # categorical
            self.collection = self.ax.scatter(
                self.xdata,
                self.ydata,
                color=facecolors,
                s=30,
                edgecolors='k',
            )
            self.xys = self.collection.get_offsets()
        else:
            self.xys = [vals for vals in zip(self.xdata, self.ydata)]
            self.collection = CircleCollection(
                sizes=(30,),
                facecolors=facecolors,
                edgecolors='k',
                offsets=self.xys,
                transOffset=self.ax.transData
            )

            self.ax.add_collection(self.collection)
        self.ax.autoscale()

        self.ax.set_ylabel(response_key)
        self.ax.set_xlabel('experiment #')
        self.ax.set_title('lasso points of interest')
        pl.autoscale()
        pl.tight_layout()
        self.cid = self.canvas.mpl_connect('button_press_event', self.onpress)

    def print_details(self, ind):
        """
        Prints the details of the job in the time-sorted list of jobs

        :param ind: pythonic index of the job in the timesort list
        """
        print('Selected index: %d' % ind)
        print(f'{MEASUREMENTS_KEY}: %s' % str(self.data[ind][MEASUREMENTS_KEY]))
        print(f'{CONDITIONS_KEY}: {self.data[ind][CONDITIONS_KEY]}')
        print(f'Experiment ID: {self.data[ind]["experiment_id"]}')
        print()

    def callback(self, verts):
        """Things to do after that mouse click"""
        facecolors = self.collection.get_facecolors()
        p = path.Path(verts)
        ind = p.contains_points(self.xys)
        inds = []
        ci = 0
        for i, val in enumerate(self.xys):
            if ind[i]:
                self.print_details(i)
                inds.append(i)
                facecolors[i] = colors[ci % len(colors)]
                ci += 1
            else:
                facecolors[i] = (0, 0, 0, 0)
        parameter_plot(
            self.data,
            self.response_key,
            *self.plot_keys,
            highlight_points=inds,
        )
        print('-' * 40, '\n')
        self.canvas.draw_idle()
        self.canvas.widgetlock.release(self.lasso)
        del self.lasso

    def onpress(self, event):
        """Triggered even on mouse click"""
        if self.canvas.widgetlock.locked():
            return
        if event.inaxes is None:
            return
        self.lasso = Lasso(
            event.inaxes,
            (event.xdata, event.ydata),
            self.callback
        )
        # acquire a lock on the widget drawing
        self.canvas.widgetlock(self.lasso)


class InteractiveParameterPlot(object):
    def __init__(self,
                 data,
                 response_key,
                 *plot_keys,
                 subplot_layout=None,
                 ):
        """
        Creates an interactive parameter plot.

        :param data: data to plot
        :param response_key: response key for the y axis
        :param plot_keys: conditions keys for the x axes
        :param subplot_layout: subplot layout (rows, columns)
        """
        self.data = data
        minmax = minima_and_maxima(data)
        # dictionary of the maximum value for that response key
        max_dct = data[minmax[MEASUREMENTS_KEY][response_key]['max_index']]
        if len(plot_keys) == 0:
            plot_keys = sorted(data[0][CONDITIONS_KEY])
        self.plot_keys = sorted(plot_keys)
        self.vals = {
            key: {
                'values': [dct[CONDITIONS_KEY][key] for dct in data]
            } for key in plot_keys
        }
        self.y = [dct[MEASUREMENTS_KEY][response_key] for dct in data]

        if subplot_layout is None:
            # automatically determine the number of subplots
            subplot_layout = _auto_subplot_shape(len(plot_keys))

        self.fig, ax = pl.subplots(
            *subplot_layout,
            sharey=True,
            # figsize=(9.6, 5.4),
            figsize=(19.2, 10.8)
        )
        # flatten axes list
        if hasattr(ax, 'shape') is False:
            self.ax = [ax]
        # todo catch this for other cases
        elif len(ax.shape) == 1:
            self.ax = [axes for axes in ax]
        elif len(ax.shape) != 1:
            self.ax = [axes for subaxes in ax for axes in subaxes]

        self.facecolors = [(0, 0, 1., 0.1) for val in data]
        for ind, key in enumerate(self.plot_keys):
            current = self.vals[key]
            if type(current['values'][0]) == str:  # categorical
                current['type'] = 'categorical'
                current['collection'] = self.ax[ind].scatter(
                    current['values'],
                    self.y,
                    color=self.facecolors,
                    edgecolors='k',
                )
                current['xys'] = current['collection'].get_offsets()
                self.ax[ind].autoscale(axis='y')
            else:  # continuous
                current['type'] = 'continuous'
                current['xys'] = list(zip(
                    current['values'],
                    self.y,
                ))
                current['collection'] = CircleCollection(
                    sizes=(30,),
                    facecolors=list(self.facecolors),
                    edgecolors='k',
                    offsets=current['xys'],
                    transOffset=self.ax[ind].transData
                )
                self.ax[ind].add_collection(current['collection'])
                self.ax[ind].autoscale(axis='y')
                local_minmax = (
                    minmax[CONDITIONS_KEY][key]['min_value'],
                    minmax[CONDITIONS_KEY][key]['max_value']
                )
                span = abs(local_minmax[1] - local_minmax[0])
                self.ax[ind].set_xlim(
                    minmax[CONDITIONS_KEY][key]['min_value'] - span * 0.02,
                    minmax[CONDITIONS_KEY][key]['max_value'] + span * 0.02
                )
            self.ax[ind].set_xlabel(f'{key}')
        # disable any axes that are unused
        if ind != len(self.ax) - 1:
            for ind in range(ind + 1, len(self.ax)):
                self.ax[ind].set_visible(False)
        # todo figure out how to set the y-label more nicely
        self.fig.text(0.01, 0.5, response_key, ha='center', va='center', rotation='vertical')
        pl.tight_layout()
        self.cid = [ax.figure.canvas.mpl_connect('button_press_event', self.onpress) for ax in self.ax]

    def print_details(self, ind):
        """
        Prints the details of the job in the time-sorted list of jobs

        :param ind: pythonic index of the job in the timesort list
        """
        print('Selected index: %d' % ind)
        print(f'{MEASUREMENTS_KEY}: %s' % str(self.data[ind][MEASUREMENTS_KEY]))
        print(f'{CONDITIONS_KEY}: {self.data[ind][CONDITIONS_KEY]}')
        print(f'Experiment ID: {self.data[ind]["experiment_id"]}')
        print()

    def callback(self, verts):
        """Things to do after that mouse click"""
        facecolors = list(self.facecolors)
        p = path.Path(verts)
        for j, key in enumerate(self.plot_keys):
            # facecolors = self.vals[j]['collection'].get_facecolors()
            ind = p.contains_points(self.vals[key]['xys'])
            ci = 0
            for i, val in enumerate(self.vals[key]['xys']):
                if ind[i]:
                    self.print_details(i)
                    facecolors[i] = colors[ci % len(colors)]
                    ci += 1
                # else:
                #     facecolors[i] = (0, 0, 0, 0)
        for j in self.vals:
            self.vals[j]['collection'].set_facecolor(facecolors)
        print('-' * 40, '\n')
        for ax in self.ax:
            ax.figure.canvas.draw_idle()
            ax.figure.canvas.widgetlock.release(self.lasso)
        del self.lasso

    def onpress(self, event):
        """Triggered even on mouse click"""
        # if self.canvas.widgetlock.locked():
        #     return
        if event.inaxes is None:
            return
        self.lasso = Lasso(
            event.inaxes,
            (event.xdata, event.ydata),
            self.callback
        )
        # acquire a lock on the widget drawing
        for ax in self.ax:
            ax.figure.canvas.widgetlock(self.lasso)


def _auto_subplot_shape(n_plots):
    """
    Automatically determines the number of rows and columns for a given number of plots.

    :param n_plots: number of total plots
    :return: rows, columns
    :rtype: tuple
    """
    if n_plots == 1:
        return 1, 1
    rows = 1
    columns = 1
    while rows * columns < n_plots:
        if rows == columns:
            rows += 1
        else:
            columns += 1
    return rows, columns


def minima_and_maxima(lst: List[dict]) -> dict:
    """
    Finds the minima and maxima of the provided dictionaries

    :param lst: list of dictionaries
    :return: dictionary of ranges
    """
    out = {}  # output dictionary
    first = lst[0]  # use first as a reference
    if MEASUREMENTS_KEY in first:
        out[MEASUREMENTS_KEY] = {}
        for key in first[MEASUREMENTS_KEY]:  # for each measurement
            values = [dct[MEASUREMENTS_KEY][key] for dct in lst]
            out[MEASUREMENTS_KEY][key] = {
                'min_value': min(values),
                'max_value': max(values),
            }
            dct = out[MEASUREMENTS_KEY][key]
            dct['min_index'] = values.index(dct['min_value'])
            dct['max_index'] = values.index(dct['max_value'])
    if CONDITIONS_KEY in first:
        out[CONDITIONS_KEY] = {}
        for key in first[CONDITIONS_KEY]:
            values = [dct[CONDITIONS_KEY][key] for dct in lst]
            out[CONDITIONS_KEY][key] = {
                'min_value': min(values),
                'max_value': max(values),
            }
            dct = out[CONDITIONS_KEY][key]
            dct['min_index'] = values.index(dct['min_value'])
            dct['max_index'] = values.index(dct['max_value'])
    return out


def plot_the_works(extrakey, *highlightinds):
    """
    Plots a subplot of the specified key beside a set of parameter subplots

    :param extrakey: key for the y-axis values of the left-most subplot
    :param highlightinds: optional highlighting of the indicies specified (indicies correspond to the pythonic index
        in the timesort list)
    :return: figure object
    """
    raise NotImplementedError('Lars still needs to fix this method to work with general data')
    if highlightinds is None:
        highlightinds = []
    fig = pl.figure(  # create base figure
        # figsize=(19.20, 10.80),
        figsize=(9.6, 5.4),
    )
    outer = gridspec.GridSpec(  # create grid
        1,
        2,
        # wspace=0.1,
        width_ratios=[1, 2],
    )
    left = gridspec.GridSpecFromSubplotSpec(
        1,
        1,
        subplot_spec=outer[0]
    )
    facecolors = [(0, 0, 1, 0.) for job in timesort]
    mainax = pl.Subplot(  # create choice subplot
        fig,
        left[0]
    )
    mainxys = [vals for vals in zip(
        [ind for ind, val in enumerate(timesort)],
        [job[extrakey] for job in timesort]
    )]
    highlightxys = [mainxys[ind] for ind in highlightinds]
    collection = CircleCollection(
        sizes=(20,),
        facecolors=facecolors,
        edgecolors=(0, 0, 0, 0.5),
        offsets=mainxys,
        transOffset=mainax.transData
    )
    highlightcol = CircleCollection(
        sizes=(30,),
        facecolors=[colors[i % len(colors)] for i, val in enumerate(highlightinds)],
        edgecolors='k',
        offsets=highlightxys,
        transOffset=mainax.transData,
    )
    mainax.add_collection(collection)
    mainax.add_collection(highlightcol)
    mainax.set_ylabel(extrakey)
    mainax.set_xlabel('injection #')
    mainax.autoscale()
    fig.add_subplot(mainax)

    indxys = [
        [(val, timesort[ind]['average']) for val in timesort[ind]['scaled parameters']] for ind in highlightinds
    ]
    inner = gridspec.GridSpecFromSubplotSpec(
        3,
        2,
        subplot_spec=outer[1],
    )
    # parameter axes
    paxes = [pl.Subplot(fig, i) for i in inner]
    areas = [job['average'] for job in timesort]
    for ind, ax in enumerate(paxes):
        xys = [vals for vals in zip([job['scaled parameters'][ind] for job in timesort], areas)]
        coll = CircleCollection(
            sizes=(20,),
            facecolors=facecolors,
            edgecolors=(0, 0, 0, 0.5),
            offsets=xys,
            transOffset=ax.transData,
        )
        for i, high in enumerate(highlightinds):
            highcol = CircleCollection(
                sizes=(30,),
                facecolors=colors[i % len(colors)],
                edgecolors='k',
                offsets=indxys[i][ind],
                transOffset=ax.transData
            )
            ax.add_collection(highcol)
        ax.add_collection(coll)
        ax.set_xlabel('%s (%s)' % (parameter_order[ind], units[ind]))
        ax.set_xlim(*current_scalars[ind])
        fig.add_subplot(ax)
        ax.autoscale(axis='y')
    outer.tight_layout(fig)
    return fig


def key_plot(data,
             y_key,
             x_label: str = 'iteration',
             ):
    """
    Plots the key vs injection number for the entire series

    :param data: list of data
    :param y_key: dictionary key defined in the jobs
    """
    if y_key in data[0][MEASUREMENTS_KEY]:
        yvals = [job[MEASUREMENTS_KEY][y_key] for job in data]
    elif y_key in data[0][CONDITIONS_KEY]:
        yvals = [job[CONDITIONS_KEY][y_key] for job in data]
    else:
        raise KeyError(f'The key {y_key} was not found in either the conditions or the measurements of the data')
    xvals = [i + 1 for i, val in enumerate(yvals)]
    fig, ax = pl.subplots(1)
    ax.scatter(
        xvals,
        yvals,
        marker='o',
        facecolors=(0, 0, 1, 0.5),
        s=10,
        picker=10,
    )
    ax.set_ylabel(y_key)
    ax.set_xlabel(x_label)
    pl.tight_layout()
    return fig, ax, [xvals, yvals]


def parameter_plot(
        data,
        response_key,
        *plot_keys,
        highlight_points=None,
        subplot_layout=None,
):
    """
    Plots specified parameters in subplots

    :param data: data
    :param response_key: response key to plot on the y axis
    :param plot_keys: conditions keys to use as x axes (each key will result in one subplot)
    :param highlight_points: indicies in the time-sorted list to highlight
    :param subplot_layout: subplot layout (rows, columns)
    :return: figure, axes
    """
    # todo allow conditions to be a response_key
    minmax = minima_and_maxima(data)
    # dictionary of the maximum value for that response key
    if response_key in minmax[MEASUREMENTS_KEY]:
        max_dct = data[minmax[MEASUREMENTS_KEY][response_key]['max_index']]
        y_values = [dct[MEASUREMENTS_KEY][response_key] for dct in data]
        max_y_value = max_dct[MEASUREMENTS_KEY][response_key]
    elif response_key in minmax[CONDITIONS_KEY]:
        max_dct = data[minmax[CONDITIONS_KEY][response_key]['max_index']]
        y_values = [dct[CONDITIONS_KEY][response_key] for dct in data]
        max_y_value = max_dct[CONDITIONS_KEY][response_key]
    if len(plot_keys) == 0:
        plot_keys = sorted(data[0][CONDITIONS_KEY])
    # elif len(plot_keys) == 1:
    #     plot_keys = list(plot_keys)
    x_values = {
        key: [dct[CONDITIONS_KEY][key] for dct in data]
        for key in plot_keys
    }

    if subplot_layout is None:
        # automatically determine the number of subplots
        subplot_layout = _auto_subplot_shape(len(plot_keys))

    fig, ax = pl.subplots(
        *subplot_layout,
        sharey=True,
        figsize=(9.6, 5.4),
    )
    # flatten axes list
    if hasattr(ax, 'shape') is False:
        ax = [ax]
    elif len(ax.shape) != 1:
        ax = [axes for subaxes in ax for axes in subaxes]

    for i, key in enumerate(sorted(plot_keys)):
        ax[i].scatter(
            x_values[key],
            y_values,
            label=f'{key}',
            marker='o',
            facecolors=(0, 0, 1, 0.5),
            s=10,
        )
        if type(max_dct[CONDITIONS_KEY][key]) == str:  # categorical
            offsets = ax[i].collections[0].get_offsets()
            for pair in zip(x_values[key], offsets):
                if pair[0] == max_dct[CONDITIONS_KEY][key]:
                    x_max_key = pair[1][0]

        else:  # continuous (normal)
            x_max_key = max_dct[CONDITIONS_KEY][key]
        coll = CircleCollection(
            sizes=(30,),
            facecolors=(0, 0, 0, 0),
            edgecolors='r',
            offsets=[
                x_max_key,
                max_y_value,
            ],
            transOffset=ax[i].transData
        )
        ax[i].add_collection(coll)
        # ax[i].scatter(
        #     maxset['parameters'][ind],
        #     maxset['average'],
        #     marker='o',
        #     # color='g',
        #     facecolors='none',
        #     edgecolors='g',
        #     s=20,
        # )
        ax[i].set_xlabel(f'{key}')
        if type(max_dct[CONDITIONS_KEY][key]) == str:  # categorical
            pl.setp(ax[i].xaxis.get_majorticklabels(), rotation=90, size=8)
        else:  # continuous (scale x)
            local_minmax = (
                minmax[CONDITIONS_KEY][key]['min_value'],
                minmax[CONDITIONS_KEY][key]['max_value']
            )
            span = abs(local_minmax[1] - local_minmax[0])
            # todo create catch for single return value
            ax[i].set_xlim(
                minmax[CONDITIONS_KEY][key]['min_value'] - span * 0.02,
                minmax[CONDITIONS_KEY][key]['max_value'] + span * 0.02
            )
    # disable any axes that are unused
    if i != len(ax) - 1:
        for i in range(i + 1, len(ax)):
            ax[i].set_visible(False)
    if highlight_points is not None:
        if type(highlight_points) == int:
            highlight_points = [highlight_points]
        for pi, point in enumerate(highlight_points):
            dct = data[point]
            for i, key in enumerate(sorted(plot_keys)):
                # col = CircleCollection(
                #     sizes=(30,),
                #     facecolors=colors[pi % len(colors)],
                #     edgecolors='k',
                #     offsets=[
                #         vals['scaled parameters'][ind],
                #         maxset['average']
                #     ],
                #     transOffset=ax[i].transData
                # )
                ax[i].scatter(
                    dct[CONDITIONS_KEY][key],
                    dct[MEASUREMENTS_KEY][response_key],
                    marker='o',
                    s=30,
                    facecolors=colors[pi % len(colors)],
                    edgecolors='k',
                )
    fig.text(0.01, 0.5, response_key, ha='center', va='center', rotation='vertical')
    pl.tight_layout()
    return fig, ax


colors = [mcolors.to_rgba(col) for col in ['red', 'orange', 'yellow', 'green', 'blue', 'violet']]


if __name__ == '__main__':
    # list of colours to use for highlighting

    font = {
        'family': 'Arial',
        'size': 14,
    }
    matplotlib.rc('font', **font)

    # todo figure out how to combine runs from multiple folders
    # target folder
    folders = [
        # 'backup_random_search_012718_1116am',  # triplicate run #1
        # 'backup_random_search_012918_0844am',  # triplicate run #2
        # 'backup_smac_013018',  # 2018-01-29 smac run
        # 'backup_phoenics_013118_1001am',  # 2018-01-30 pheonics run (truncated due to bug)
        # 'backup_spearmint_020118_1422pm',  # 2018-01-31 spearmin run
        'backup_020818_2008pm',  # random exploration run 1
        'backup_2018-02-13',  # random exploration run 2
    ]
    folder = os.path.join(
        CHEMOSPATH,
        'output',
        # 'backup_2018-03-30',  # first ranked optimization
        # 'backup_random_search_012718_1116am',  # triplicate run #1
        # 'backup_random_search_012918_0844am',  # triplicate run #2
        # 'backup_smac_013018',  # 2018-01-29 smac run
        'backup_phoenics_013118_1001am',  # 2018-01-30 pheonics run (truncated due to bug)
        # 'backup_spearmint_020118_1422pm',  # 2018-01-31 spearmin run
        # 'backup_020818_2008pm',  # random exploration run 1 (load this one for entire run)
        # 'backup_2018-02-13',  # random exploration run 2
    )

    # try:
    #     with open(os.path.join(folder, 'lars_run_summary.summary'), 'rb') as preprocessed:
    #         loaddct = pickle.load(preprocessed)
    #         timesort = loaddct['timesort']
    #         maxset = loaddct['maxset']
    #         parameter_order = loaddct['parameter_order']
    #         current_scalars = loaddct['current_scalars']
    #         units = loaddct['units']
    #     print('Loaded from preprocessed')
    #
    # except FileNotFoundError:
    #
    #     timesort, maxset = retrieve_files(folder)
    #
    #     with open(os.path.join(folder, 'lars_run_summary.sum'), 'wb') as preprocessed:
    #         pickle.dump(
    #             {
    #                 'timesort': timesort,
    #                 'maxset': maxset,
    #                 'parameter_order': parameter_order,
    #                 'current_scalars': current_scalars,
    #                 'units': units,
    #             },
    #             preprocessed
    #         )
