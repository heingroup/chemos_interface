"""automatically organizes the output directory, moving pickle files into their appropriate session folders"""

from chemos_interface.util import organize_directory

organize_directory()
