"""
An example file to illustrate how the chemos interface tools maybe be used to retrieve instructions and return results
 to/from ChemOS. This script is an example of synchronous loops.

See loop_integration.md for additional details.

Prerequisites: see the "Setup" portion of the README and ensure that `chemospath` is defined as an environment variable.
"""
import random
import logging
from chemos_interface.config import CONDITIONS_KEY, MEASUREMENTS_KEY
from chemos_interface.io import get_oldest_parameters, write_completed_parameters

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('Example sync loop integration')


def perform_experiment(**coniditions):
    """
    A dummy method which "performs" an experiment based on the provided ChemOS parameters.

    :param parameters: named key, value pairs provided by ChemOS
    :return: response value
    """
    # here you would set acquisition parameters, assign values, etc.
    for key, value in coniditions.items():
        logger.info(f'performing action with {key}: {value}')
    return random.random()


# synchronous example
while True:
    # get the oldest parameters (first ones proposed by ChemOS)
    parameters = get_oldest_parameters(
        wait_for=True,  # waits for appearance of a file
        move_file=True,  # moves the input file into the "In Progress" folder
    )
    # perform the experiment
    response = perform_experiment(
        **parameters[CONDITIONS_KEY]  # the conditions for the experiment are in a subdictionary
    )
    # add the response value to the measurements subdictionary
    parameters[MEASUREMENTS_KEY] = {
        'response': response,  # the response keys must be the same as those expected by ChemOS
    }

    # writes the completed parameters to file and moves the file to the Results folder for ChemOS to read
    write_completed_parameters(
        parameters,
    )
